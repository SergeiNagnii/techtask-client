# Tech task - search-client

### Prerequisites

built and started search-server project

### Installing

```
mvn clean install
```

### Running

Run project (either from IDE or by java -jar command)

Also search project should be built and started

navigate to http://localhost:8080