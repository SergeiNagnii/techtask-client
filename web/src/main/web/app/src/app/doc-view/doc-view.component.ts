import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DocServiceService} from '../service/doc-service.service';

@Component({
    selector: 'doc-view',
    templateUrl: './doc-view.component.html',
    styleUrls: ['./doc-view.component.scss']
})
export class DocViewComponent implements OnInit {

    doc = {key: '', content: ''};

    @Input()
    docKey = '';

    @Output()
    errorEventEmitter = new EventEmitter<any>();

    constructor(private docServiceService: DocServiceService) {
    }

    ngOnInit() {
        this.docServiceService.view(this.docKey)
            .subscribe(res => {
                this.doc = res;
            }, (err) => this.errorEventEmitter.emit(err));
    }
}
