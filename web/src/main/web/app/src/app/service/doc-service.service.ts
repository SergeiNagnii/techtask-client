import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocServiceService {

  constructor(private httpClient: HttpClient) { }

  addNewDocument(value: any): Observable<any> {
    return this.httpClient.post('/documents', value);
  }

  search(tokens: string): Observable<any> {
    let params = new HttpParams().set('tokens', `${tokens.trim().split(' ')}`);
    return this.httpClient.get('/documents/tokens', {params: params});
  }

  view(key: string): Observable<any> {
    let params = new HttpParams().set('key', key);
    return this.httpClient.get('/documents/key', {params: params});
  }
}
