import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent {

    @Input()
    searchResult = [];

    @Output()
    isViewDoc = new EventEmitter<string>();

    constructor() {
    }

    onViewDoc(key: string) {
        this.isViewDoc.emit(key);
    }
}
