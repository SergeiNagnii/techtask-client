import {Component} from '@angular/core';
import {DocServiceService} from './service/doc-service.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    searchResult: any[];
    docKey = '';

    private isAddDoc = false;
    private isViewDoc = false;
    private isAlertClosed = true;

    constructor(private docServiceService: DocServiceService) {
    }

    search(input) {
        this.isAlertClosed = true;
        this.isAddDoc = false;
        this.isViewDoc = false;
        this.docServiceService.search(input.value).subscribe(res => {
            this.searchResult = res.documentKeys;
        }, (err) => this.isAlertClosed = false);
    }

    onAddDocs() {
        this.isAlertClosed = true;
        this.isViewDoc = false;
        this.isAddDoc = true;
    }

    onViewDoc(value: string) {
        this.docKey = value;
        this.isViewDoc = true;
    }

    close() {
        this.isAlertClosed = true;
    }

    showAlert(val: boolean) {
        this.isAlertClosed = !val;
    }
}
