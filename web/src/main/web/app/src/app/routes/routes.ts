import {Routes} from "@angular/router";
import {AddDocsComponent} from "../add-docs/add-docs.component";
import {SearchResultComponent} from "../search-result/search-result.component";
import {DocViewComponent} from "../doc-view/doc-view.component";

export const appRoutes: Routes = [
  {path: 'docs/add', component: AddDocsComponent},
  {path: 'docs/view/:key', component: DocViewComponent},
  {path: 'docs/search/:tokens', component: SearchResultComponent}];
