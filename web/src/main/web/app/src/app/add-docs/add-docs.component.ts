import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DocServiceService} from '../service/doc-service.service';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'add-docs',
  templateUrl: './add-docs.component.html',
  styleUrls: ['./add-docs.component.scss']
})
export class AddDocsComponent implements OnInit {

  form: FormGroup;
  constructor(private docServiceService: DocServiceService) { }

  @Output()
  errorEventEmitter = new EventEmitter<any>();
  @Output()
  successEventEmitter = new EventEmitter<any>();

  ngOnInit() {

  }

  onAdd(doc) {
    console.log(doc.value);
    this.docServiceService.addNewDocument(doc.value).subscribe(
        () => this.successEventEmitter.emit(),
        (err) => this.errorEventEmitter.emit(err));
    doc.reset();
  }

}
