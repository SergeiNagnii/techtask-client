package com.techtask.search.client.rest.service.impl;

import com.techtask.search.client.rest.dto.DocumentDto;
import com.techtask.search.client.rest.dto.ResponseDocumentKeysDto;
import com.techtask.search.client.rest.service.SearchDocsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SearchDocsServiceImpl implements SearchDocsService{

    private RestTemplate restTemplate;
    private String url;

    public SearchDocsServiceImpl(@Value("${search.docs.url}")String url, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.url = url;
    }

    @Override
    public void saveDocument(DocumentDto documentDto) {
        restTemplate.postForObject(url, documentDto, DocumentDto.class);
    }

    @Override
    public DocumentDto getDocumentByKey(String key) {
        return restTemplate.getForObject(String.format("%s/keys?documentKey=%s", url, key), DocumentDto.class);
    }

    @Override
    public ResponseDocumentKeysDto getDocumentByTokens(String tokens) {
        return restTemplate.getForObject(String.format("%s/tokens?tokens=%s", url, tokens), ResponseDocumentKeysDto.class);
    }
}
