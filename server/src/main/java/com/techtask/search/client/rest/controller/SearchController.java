package com.techtask.search.client.rest.controller;

import com.techtask.search.client.rest.dto.DocumentDto;
import com.techtask.search.client.rest.dto.ResponseDocumentKeysDto;
import com.techtask.search.client.rest.service.SearchDocsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/documents")
public class SearchController {

    private final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private SearchDocsService searchDocsService;

    public SearchController(SearchDocsService searchDocsService) {
        this.searchDocsService = searchDocsService;
    }

    @GetMapping("/key")
    public DocumentDto getDocumentsByKey(@RequestParam String key) {
        return searchDocsService.getDocumentByKey(key);
    }

    @GetMapping("/tokens")
    public ResponseDocumentKeysDto getDocumentsByTokens(@RequestParam String tokens) {
        return searchDocsService.getDocumentByTokens(tokens);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public void saveDocument(@RequestBody DocumentDto doc) {
         searchDocsService.saveDocument(doc);
    }

    @ExceptionHandler
    public ResponseEntity<String> onException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
