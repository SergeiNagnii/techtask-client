package com.techtask.search.client.rest.service;

import com.techtask.search.client.rest.dto.DocumentDto;
import com.techtask.search.client.rest.dto.ResponseDocumentKeysDto;

public interface SearchDocsService {

    void saveDocument(DocumentDto documentDto);

    DocumentDto getDocumentByKey(String key);

    ResponseDocumentKeysDto getDocumentByTokens(String tokens);
}
