package com.techtask.search.client.rest.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techtask.search.client.rest.dto.DocumentDto;
import com.techtask.search.client.rest.dto.ResponseDocumentKeysDto;
import com.techtask.search.client.rest.service.SearchDocsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchDocsServiceImplTest {

    @Value("${search.docs.url}")
    private String url;

    @Autowired
    private SearchDocsService searchDocsService;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void getDocumentByKey() throws URISyntaxException, JsonProcessingException {
        mockServer.expect(ExpectedCount.once(),
                          requestTo(new URI(String.format("%s/keys?documentKey=doc1", url))))
                  .andExpect(method(HttpMethod.GET))
                  .andRespond(withStatus(HttpStatus.OK)
                  .contentType(MediaType.APPLICATION_JSON)
                  .body(mapper.writeValueAsBytes(new DocumentDto("doc1", "1 2 3")))
                );
        DocumentDto response = searchDocsService.getDocumentByKey("doc1");
        mockServer.verify();

        assertEquals("doc1", response.getKey());
    }

    @Test
    public void getDocumentByTokens() throws URISyntaxException, JsonProcessingException {
        Set<String> responseDocKeys = new HashSet<>();
        responseDocKeys.add("doc1");
        mockServer.expect(ExpectedCount.once(),
                          requestTo(new URI(String.format("%s/tokens?tokens=2,3", url))))
                  .andExpect(method(HttpMethod.GET))
                  .andRespond(withStatus(HttpStatus.OK)
                  .contentType(MediaType.APPLICATION_JSON)
                  .body(mapper.writeValueAsBytes(new ResponseDocumentKeysDto(responseDocKeys)))
                );
        ResponseDocumentKeysDto response = searchDocsService.getDocumentByTokens("2,3");
        mockServer.verify();

        assertEquals("doc1", response.getDocumentKeys().iterator().next());
    }

    @Test
    public void saveDocument() throws URISyntaxException {
        mockServer.expect(ExpectedCount.once(),
                requestTo(new URI(String.format("%s", url))))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                );
        searchDocsService.saveDocument(new DocumentDto("doc1", "123"));
        mockServer.verify();
    }

}
